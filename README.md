# eSports data platform

A master repository to get all the necessary modules for setting up the game server and client.


# Instructions
* [CSGO dedicated server](https://gitlab.unige.ch/sims/esports-data-platform/-/blob/master/server/csgo_ds_setup.md)
* [Client capture setup](https://gitlab.unige.ch/sims/esports-data-platform/-/blob/master/client/capture_setup.md)
* Follow instructions of respective submodules