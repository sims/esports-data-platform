Rem mouse.exe takes one parameter: player ID
start ./Input/mouse.exe P0 -wo ./Input/
Rem keyboard.exe takes one parameter: player ID
start ./Input/keyboard.exe P0 -wo ./Input/
Rem Realsense takes 3 parameters: Player ID, resolution (look at list of possible values), emitter enable
start ./RealSense/IntelRealSense-FrameCapture.exe P0 640x480 false -wo ./RealSense/
Rem PressureMat takes 2 parameters: Player ID, COM port
start ./PressureMat/SensingTex-PressureMat.exe P0 0 -wo ./PressureMat/
Rem Bitalino takes 4 parameters: Player ID, Bitalino device MAC address, binary channel select mask [ECG EDA Resp EMG EGG], sampling rate in Hz
start ./BitalinoRecorder/BitalinoRecorder.exe P0 00:00:00:00 00000 10 -wo ./BitalinoRecorder/
Rem TobiiPro takes 1 parameter: the configuration file where you can define Player ID, device address
start ./TobiiPro/TobiiPro.exe TobiiPro.cfg