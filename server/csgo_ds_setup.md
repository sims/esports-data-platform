# steamcmd
https://developer.valvesoftware.com/wiki/SteamCMD#Downloading_SteamCMD

> ```
> sudo useradd -m steam
> cd /home/steam
> sudo apt install steamcmd
> ```

make sure steam user console uses bash to make our lives easier
```
sudo -u steam chsh -s /bin/bash
```
set passwd of steam user
```
sudo passwd steam
```

# Dedicated server
https://developer.valvesoftware.com/wiki/Counter-Strike:_Global_Offensive_Dedicated_Servers

as steam user
```
su - steam
```
paswd `steam`

> Start `steamcmd`
> ```
> force_install_dir /home/steam/csgo-ds
> app_update
> login anonymous
> app_update 740 validate
> ```
>
> To just update:
> ```
> force_install_dir /home/steam/csgo-ds
> app_update 740
> ```

Default install dir if you dont `force_install_dir` is `/home/steam/.steam/SteamApps/common`

## autoexec.cfg
copy to `csgo_ds/csgo/cfg/`

some more cvars:
https://developer.valvesoftware.com/wiki/List_of_CS:GO_Cvars

## server.cfg
copy to `csgo_ds/csgo/cfg/`


## gamemode_deathmatch_server.cfg
copy to `csgo_ds/csgo/cfg/`

## gamemodes_server.txt
copy to `csgo_ds/csgo/`

## start server

deathmatch (LAN only)
```
./srcds_run -game csgo -console -condebug  -usercon +game_type 1 +game_mode 2 +map de_shortdust +mapgroup mg_short
```

deathmatch (using a steam server account)
```
./srcds_run -game csgo -console -condebug -usercon +game_type 1 +game_mode 2 +map de_shortdust +mapgroup mg_short +sv_setsteamaccount ECB9302C8FBBC172C5DEF03C3926EF38 -net_port_try 1
```

## log location
`/home/steam/csgo_ds/csgo/logs`

## maps for 1v1 (or 2v2)
* shortdust
* shortnuke
* shorttrain

## useful commands
`mp_warmup_start` - starts a new warmup round
